

function loadAllAnimes() {
    var responseData = null;
    var data = fetch('https://api.jikan.moe/v3/search/anime?q=natsume')
        .then((response) => {
            console.log(response)
            return response.json()
        }).then((json) => {
            // console.log(json)
            responseData = json
            var resultElmennt = document.getElementById('result')
            resultElmennt.innerHTML = JSON.stringify(json, null, 2)
        })
}

async function loadAllAnimeAsync() {
    let response = await fetch('https://api.jikan.moe/v3/search/anime?q=natsume')
    let data = await response.json()
    var resultElmennt = document.getElementById('result')

    return data
}

function createResultAnime(data) {
    let resultElmennt = document.getElementById('resultAnime')
    resultElmennt.innerHTML = ''
    console.log(data)

    data.then((json) => {
        //add data 
        for (let i = 0; i < json["results"].length; i++) {
            var currentData = json["results"][i]

            let card = document.createElement('div')
            card.setAttribute('class', 'card')
            card.style.marginBottom = '2em'
            resultElmennt.appendChild(card)

            //create row card
            let row = document.createElement('div')
            row.setAttribute('class', 'row')
            card.appendChild(row)
            var br = document.createElement('br')
            row.appendChild(br)
            //create image card
            let imgNode = document.createElement('div')
            imgNode.setAttribute('class', 'col-md-4')
            row.appendChild(imgNode)

            let img = document.createElement('img')
            img.setAttribute('src', currentData['image_url'])
            img.setAttribute('class', 'w-100')
            img.style.width = '300px'
            img.style.height = '250px'
            imgNode.appendChild(img)

            //create info card
            let infoNode = document.createElement('div')
            infoNode.setAttribute('class', 'col-md-8 px-3')
            row.appendChild(infoNode)

            let infoCard = document.createElement('div')
            infoCard.setAttribute('class', 'card-body px-3')
            infoNode.appendChild(infoCard)

            let title = document.createElement('h4')
            title.innerText = currentData['title']
            title.setAttribute('class', 'card-title')
            infoCard.appendChild(title)

            let text = document.createElement('p')
            text.innerText = currentData['synopsis']
            text.setAttribute('class', 'card-text')
            infoCard.appendChild(text)

            let episodes = document.createElement('p')
            episodes.innerHTML = 'Episodes : ' + currentData['episodes']
            infoCard.appendChild(episodes)

            let score = document.createElement('p')
            score.innerHTML = 'Score : ' + currentData['score']
            infoCard.appendChild(score)

        }
    })
}

async function loadOneAnime() {
    let name = document.getElementById('queryId').value
    if (name != '' && name != null) {
        let response = await fetch('https://api.jikan.moe/v3/search/anime?q=' + name)
        return response.json()
    }
}
