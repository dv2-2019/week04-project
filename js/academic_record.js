function addTable() {
    //create table thead
    //1st year Semester1 2560
    var sem11 = document.createElement('h4')
    sem11.innerHTML = 'Semester 1 / 2560'
    var placeholder = document.getElementById('semester12')
    placeholder.appendChild(sem11)

    var table = document.createElement('table');
    table.setAttribute('class', 'table table-bordered table-hover');

    var arrHead = new Array();
    arrHead = ['No', 'Course no', 'Course Title', 'Credit', 'Grade']

    var thead = document.createElement('thead')
    thead.setAttribute('class', 'thead-dark')
    table.appendChild(thead)

    var tr = document.createElement('tr')
    thead.appendChild(tr)

    for (var h = 0; h < arrHead.length; h++) {
        var th = document.createElement('th');
        th.innerHTML = arrHead[h];
        tr.appendChild(th);
    }

    //create table tbody
    var arrValue = new Array();
    arrValue.push(['1', '001101', 'FUNDAMENTAL ENGLISH 1', '3.00', 'C+']);
    arrValue.push(['2', '206113', '	CAL FOR SOFTWARE ENGINEERING', '3.00', 'C']);
    arrValue.push(['3', '751100', 'ECONOMICS FOR EVERYDAY LIFE', '3.00', 'W']);
    arrValue.push(['4', '951100', 'MODERN LIFE AND ANIMATION', '3.00', 'A']);
    arrValue.push(['5', '953103', 'PROGRAMMING LOGICAL THINKING', '2.00', 'C+']);
    arrValue.push(['6', '953211', '	COMPUTER ORGANIZATION', '3.00', 'B']);
    arrValue.push(['ผลการศึกษา/Record', 'หน่วยกิตที่ลง / CA', '	หน่วยกิตที่ได้ / CE', 'เกรดเฉลี่ย / GPA', 'GPA รวม']);
    arrValue.push(['ภาคการศึกษานี้ / Semester 1/2560', '17.00', '14.00', '2.82', '2.82']);

    var tbody = document.createElement('tbody')
    table.appendChild(tbody)
    for (var c = 0; c <= arrValue.length - 1; c++) {
        tr = table.insertRow(-1);

        for (var j = 0; j < arrHead.length; j++) {
            var th = document.createElement('th');
            th.setAttribute('scope', 'row')
            tr.appendChild(th)
            th.innerHTML = arrValue[c][j];

            for (var d = 1; d < arrHead.length; d++) {
                var td = document.createElement('td')
                tr.setAttribute('class', 'table-light')
                if (c == 6) {
                    tr.setAttribute('class', 'table-secondary')
                }
                tr.appendChild(td)
                td.innerHTML = arrValue[c][d]
            } break
        }
        tbody.appendChild(tr)
    }
    var placeholder = document.getElementById('resultGPA1')
    placeholder.appendChild(table)

    //create table thead
    //1st year Semester2 2560
    var sem12 = document.createElement('h4')
    sem12.innerHTML = 'Semester 2 / 2560'
    var placeholder = document.getElementById('semester22')
    placeholder.appendChild(sem12)

    var table2 = document.createElement('table');
    table2.setAttribute('class', 'table table-bordered table-hover');

    var arrHead2 = new Array();
    arrHead2 = ['No', 'Course no', 'Course Title', 'Credit', 'Grade']

    var thead2 = document.createElement('thead')
    thead2.setAttribute('class', 'thead-dark')
    table2.appendChild(thead2)

    var tr2 = document.createElement('tr')
    thead2.appendChild(tr2)

    for (var h2 = 0; h2 < arrHead2.length; h2++) {
        var th2 = document.createElement('th');
        th2.innerHTML = arrHead2[h2];
        tr2.appendChild(th2);
    }

    //create table2 tbody
    var arrValue2 = new Array();
    arrValue2.push(['1', '001102', 'FUNDAMENTAL ENGLISH 2', '3.00', 'C+']);
    arrValue2.push(['2', '011251', 'LOGIC', '3.00', 'C+']);
    arrValue2.push(['3', '953102', 'ADT & PROBLEM SOLVING', '3.00', 'B']);
    arrValue2.push(['4', '953104', 'WEB UI DESIGN & DEVELOP', '2.00', 'B']);
    arrValue2.push(['5', '953202', 'INTRODUCTION TO SE', '3.00', 'C+']);
    arrValue2.push(['6', '953231', 'OBJECT ORIENTED PROGRAMMING', '3.00', 'C+']);
    arrValue2.push(['7', '955100', 'LEARNING THROUGH ACTIVITIES 1', '1.00', 'A']);
    arrValue2.push(['ผลการศึกษา/Record', 'หน่วยกิตที่ลง / CA', '	หน่วยกิตที่ได้ / CE', 'เกรดเฉลี่ย / GPA', 'GPA รวม']);
    arrValue2.push(['ภาคการศึกษานี้ / Semester 2/2560', '18.00', '18.00', '2.72', '2.77']);

    var tbody2 = document.createElement('tbody')
    table2.appendChild(tbody2)
    for (var c = 0; c <= arrValue2.length - 1; c++) {
        tr2 = table2.insertRow(-1);

        for (var j = 0; j < arrHead2.length; j++) {
            var th2 = document.createElement('th');
            th2.setAttribute('scope', 'row')
            tr2.appendChild(th2)
            th2.innerHTML = arrValue2[c][j];

            for (var d = 1; d < arrHead2.length; d++) {
                var td2 = document.createElement('td')
                tr2.setAttribute('class', 'table-light')
                if (c == 7) {
                    tr2.setAttribute('class', 'table-secondary')
                }
                tr2.appendChild(td2)
                td2.innerHTML = arrValue2[c][d]
            } break
        }
        tbody2.appendChild(tr2)
    }
    var placeholder = document.getElementById('resultGPA2')
    placeholder.appendChild(table2)


    //create table thead
    //2nd year Semester1 2561
    var sem31 = document.createElement('h4')
    sem31.innerHTML = 'Semester 1 / 2561'
    var placeholder = document.getElementById('semester31')
    placeholder.appendChild(sem31)

    var table3 = document.createElement('table');
    table3.setAttribute('class', 'table table-bordered table-hover');

    var arrHead3 = new Array();
    arrHead3 = ['No', 'Course no', 'Course Title', 'Credit', 'Grade']

    var thead3 = document.createElement('thead')
    thead3.setAttribute('class', 'thead-dark')
    table3.appendChild(thead3)

    var tr3 = document.createElement('tr')
    thead3.appendChild(tr3)

    for (var h3 = 0; h3 < arrHead3.length; h3++) {
        var th3 = document.createElement('th');
        th3.innerHTML = arrHead3[h3];
        tr3.appendChild(th3);
    }

    //create table3 tbody
    var arrValue3 = new Array();
    arrValue3.push(['1', '001201', 'CRIT READ AND EFFEC WRITE', '3.00', 'D+']);
    arrValue3.push(['2', '013110', 'PSYCHOLOGY AND DAILY LIFE', '3.00', 'B+']);
    arrValue3.push(['3', '206281', 'DISCRETE MATHEMATICS', '3.00', 'C+']);
    arrValue3.push(['4', '953212', 'DB SYS & DB SYS DESIGN', '3.00', 'B']);
    arrValue3.push(['5', '953233', 'PROGRAMMING METHODOLOGY', '3.00', 'C+']);
    arrValue3.push(['6', '953261', 'INTERACTIVE WEB DEVELOPMENT', '2.00', 'C+']);
    arrValue3.push(['7', '953361', 'COMP NETWORK & PROTOCOLS', '3.00', 'D']);
    arrValue3.push(['ผลการศึกษา/Record', 'หน่วยกิตที่ลง / CA', '	หน่วยกิตที่ได้ / CE', 'เกรดเฉลี่ย / GPA', 'GPA รวม']);
    arrValue3.push(['ภาคการศึกษานี้ / Semester 1/2561', '20.00', '20.00', '2.35', '2.61']);

    var tbody3 = document.createElement('tbody')
    table3.appendChild(tbody3)
    for (var c = 0; c <= arrValue3.length - 1; c++) {
        tr3 = table3.insertRow(-1);

        for (var j = 0; j < arrHead3.length; j++) {
            var th3 = document.createElement('th');
            th3.setAttribute('scope', 'row')
            tr3.appendChild(th3)
            th3.innerHTML = arrValue3[c][j];

            for (var d = 1; d < arrHead3.length; d++) {
                var td3 = document.createElement('td')
                tr3.setAttribute('class', 'table-light')
                if (c == 7) {
                    tr3.setAttribute('class', 'table-secondary')
                }
                tr3.appendChild(td3)
                td3.innerHTML = arrValue3[c][d]
            } break
        }
        tbody3.appendChild(tr3)
    }
    var placeholder = document.getElementById('resultGPA3')
    placeholder.appendChild(table3)


    //create table thead
    //2nd year Semester2 2561
    var sem32 = document.createElement('h4')
    sem32.innerHTML = 'Semester 2 / 2561'
    var placeholder = document.getElementById('semester32')
    placeholder.appendChild(sem32)

    var table4 = document.createElement('table');
    table4.setAttribute('class', 'table table-bordered table-hover');

    var arrHead4 = new Array();
    arrHead4 = ['No', 'Course no', 'Course Title', 'Credit', 'Grade']

    var thead4 = document.createElement('thead')
    thead4.setAttribute('class', 'thead-dark')
    table4.appendChild(thead4)

    var tr4 = document.createElement('tr')
    thead4.appendChild(tr4)

    for (var h4 = 0; h4 < arrHead4.length; h4++) {
        var th4 = document.createElement('th');
        th4.innerHTML = arrHead4[h4];
        tr4.appendChild(th4);
    }

    //create table4 tbody
    var arrValue4 = new Array();
    arrValue4.push(['1', '001225', 'ENGL IN SCIENCE & TECH CONT', '3.00', 'C+']);
    arrValue4.push(['2', '206255', 'MATH FOR SOFTWARE TECH', '3.00', 'B+']);
    arrValue4.push(['3', '953201', 'ALGO DESIGN & ANALYSIS', '3.00', 'C']);
    arrValue4.push(['4', '953214', 'OS & PROG LANG PRINCIPLES', '3.00', 'B+']);
    arrValue4.push(['5', '953232', 'OO ANALYSIS & DESIGN', '3.00', 'B']);
    arrValue4.push(['6', '953234', 'ADVANCED SOFTWARE DEVELOPMENT', '3.00', 'B+']);
    arrValue4.push(['7', '955200', 'LEARNING THROUGH ACTIVITIES 2', '1.00', 'A']);
    arrValue4.push(['ผลการศึกษา/Record', 'หน่วยกิตที่ลง / CA', '	หน่วยกิตที่ได้ / CE', 'เกรดเฉลี่ย / GPA', 'GPA รวม']);
    arrValue4.push(['ภาคการศึกษานี้ / Semester 2/2561', '19.00', '19.00', '3.05', '2.73']);

    var tbody4 = document.createElement('tbody')
    table4.appendChild(tbody4)
    for (var c = 0; c <= arrValue4.length - 1; c++) {
        tr4 = table4.insertRow(-1);

        for (var j = 0; j < arrHead4.length; j++) {
            var th4 = document.createElement('th');
            th4.setAttribute('scope', 'row')
            tr4.appendChild(th4)
            th4.innerHTML = arrValue4[c][j];

            for (var d = 1; d < arrHead4.length; d++) {
                var td4 = document.createElement('td')
                tr4.setAttribute('class', 'table-light')
                if (c == 7) {
                    tr4.setAttribute('class', 'table-secondary')
                }
                tr4.appendChild(td4)
                td4.innerHTML = arrValue4[c][d]
            } break
        }
        tbody4.appendChild(tr4)
    }
    var placeholder = document.getElementById('resultGPA4')
    placeholder.appendChild(table4)


    //create table thead
    //3rd year Semester1 2562
    var sem41 = document.createElement('h4')
    sem41.innerHTML = 'Semester 1 / 2562'
    var placeholder = document.getElementById('semester41')
    placeholder.appendChild(sem41)

    var table5 = document.createElement('table');
    table5.setAttribute('class', 'table table-bordered table-hover');

    var arrHead5 = new Array();
    arrHead5 = ['No', 'Course no', 'Course Title', 'Credit', 'Grade']

    var thead5 = document.createElement('thead')
    thead5.setAttribute('class', 'thead-dark')
    table5.appendChild(thead5)

    var tr5 = document.createElement('tr')
    thead5.appendChild(tr5)

    for (var h5 = 0; h5 < arrHead5.length; h5++) {
        var th5 = document.createElement('th');
        th5.innerHTML = arrHead5[h5];
        tr5.appendChild(th5);
    }

    //create table5 tbody
    var arrValue5 = new Array();
    arrValue5.push(['1', '011151', 'REASONING', '3.00', 'A']);
    arrValue5.push(['2', '208263', 'ELEMENTARY STATISTICS', '3.00', 'D+']);
    arrValue5.push(['3', '259109', 'TELECOM IN THAILAND', '3.00', 'A']);
    arrValue5.push(['4', '953321', 'SOFTWARE REQ ANALYSIS', '3.00', 'B']);
    arrValue5.push(['5', '953332', 'SOFTWARE DESIGN & ARCH', '3.00', 'C']);
    arrValue5.push(['6', '953331', 'COMPO-BASED SOFTWARE DEV', '3.00', 'B']);
    arrValue5.push(['ผลการศึกษา/Record', 'หน่วยกิตที่ลง / CA', '	หน่วยกิตที่ได้ / CE', 'เกรดเฉลี่ย / GPA', 'GPA รวม']);
    arrValue5.push(['ภาคการศึกษานี้ / Semester 1/2562', '18.00', '18.00', '2.92', '2.76']);
    arrValue5.push(['สะสมทั้งหมด / Cumulative', ' ', '89.00', '2.76', ' ']);

    var tbody5 = document.createElement('tbody')
    table5.appendChild(tbody5)
    for (var c = 0; c <= arrValue5.length - 1; c++) {
        tr5 = table5.insertRow(-1);

        for (var j = 0; j < arrHead5.length; j++) {
            var th5 = document.createElement('th');
            th5.setAttribute('scope', 'row')
            tr5.appendChild(th5)
            th5.innerHTML = arrValue5[c][j];

            for (var d = 1; d < arrHead5.length; d++) {
                var td5 = document.createElement('td')
                tr5.setAttribute('class', 'table-light')
                if (c == 6) {
                    tr5.setAttribute('class', 'table-secondary')
                }
                tr5.appendChild(td5)
                td5.innerHTML = arrValue5[c][d]
            } break
        }
        tbody5.appendChild(tr5)
    }
    var placeholder = document.getElementById('resultGPA5')
    placeholder.appendChild(table5)



}
