

function loadAllAnimes() {
    var responseData = null;
    var data = fetch('https://api.jikan.moe/v3/search/anime?q=naruto')
        .then((response) => {
            console.log(response)
            return response.json()
        }).then((json) => {
            // console.log(json)
            responseData = json
            var resultElmennt = document.getElementById('result')
            resultElmennt.innerHTML = JSON.stringify(json, null, 2)
        })
}

async function loadAllAnimeAsync() {
    let response = await fetch('https://api.jikan.moe/v3/search/anime?q=naruto')
    let data = await response.json()
    var resultElmennt = document.getElementById('result')

    return data
}

function createResultTable(data) {
    let resultElmennt = document.getElementById('resultAnime')
    let tableNode = document.createElement('table')
    resultElmennt.innerHTML = ''
    resultElmennt.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')


    //create the header
    let tableHeadNode = document.createElement('thead')
    tableHeadNode.setAttribute('class','thead-dark')
    tableNode.appendChild(tableHeadNode)
    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Title'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Episodes'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) => {
        //add data 
        for (let i = 0; i < json["results"].length; i++) {
            var currentData = json["results"][i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)

            var columnNode = null; 
            columnNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['image_url'])
            imageNode.style.width = '300px'
            imageNode.style.height = '250px'
            dataRow.appendChild(imageNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['title']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['episodes']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['synopsis']
            dataRow.appendChild(columnNode)

           
        }
    })
}

async function loadOneMovie() {
    let name = document.getElementById('queryId').value
    if (name != '' && name != null) {
        let response = await fetch('https://api.jikan.moe/v3/search/anime?q=/' + name)
        return response.json()
    }
}
